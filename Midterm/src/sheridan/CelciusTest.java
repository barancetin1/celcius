package sheridan;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CelciusTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}
	
	@Test
	public void testIsPalindrome() {
		assertEquals(7, Celcius.convertFromFahrenheit(45));
	}
	
	@Test
	public void testConversionException() {
		assertNotEquals(-573, Celcius.convertFromFahrenheit(-1000));
	}
	
	@Test
	public void testCecliusBoundaryIn() {
		assertEquals(-14, Celcius.convertFromFahrenheit(6));
	}
	
	@Test
	public void testCelciusBoundaryOut() {
		assertNotEquals(-13, Celcius.convertFromFahrenheit(7));
	}

}
