package sheridan;

public class Celcius {

	public static void main(String[] args) {

		
		System.out.println(convertFromFahrenheit(-1000));
		
	}
	
	public static int convertFromFahrenheit(int n) {
		if(n < -500) {
			return 0;
		}
		else {
			double celcius = Math.round((n - 32.0) * 5.0 /9.0);
			return (int)celcius;
		}
		
	}

}
